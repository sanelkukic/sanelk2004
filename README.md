## 👋 Hey there!
I'm Sanel! I'm 17 years old. I&#39;m a software developer and Linux system administrator.

Here's some fun facts about me:

- 🌎 I speak the following languages: `Bosnian`, `English`
- 💻 I code in the following programming languages: `JavaScript`, `Python`, `C#`, `Java`
- 💬 My pronouns are: `He`, `Him`
- :heart: Check out my [GitHub Sponsors](https://github.com/sponsors/sanelk2004) profile!
- :school: I graduated from Florida Virtual School! Class of 2021
- :bosnia_herzegovina: I was born and raised in the United States, but my parents were born and raised in Bosnia!
- :cat: I have two pet cats, one named Kitty and one named Mini! You can view pictures of them by [clicking here](https://cats.sanelkukic.us.eu.org)

---

<details open>
<summary>👀 <b>My GitHub stats</b></summary>
<br>
<p align="center">
<a href="#"><img src="https://github-readme-stats.vercel.app/api?username=sanelk2004&show_icons=true&theme=dracula&line_height=27&count_private=true"></a>
<a href="#"><img src="https://github-readme-stats.vercel.app/api/top-langs/?username=sanelk2004&theme=dracula&count_private=true&layout=compact"></a>
</p>
</details>

---

<details open>
<summary>💬 <b>My Discord profile</b></summary>
<br>

[![Discord Presence](https://lanyard-profile-readme.vercel.app/api/588852269177372685?theme=light)](https://discord.com/users/588852269177372685)

</details>

---

<a href="https://www.buymeacoffee.com/sanelkukic" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-green.png" alt="Buy Me A Coffee" style="height: 60px !important;width: 217px !important;" ></a>

<a href="https://liberapay.com/sanelkukic/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

---

Thanks for stopping by my GitHub profile! Hope you enjoyed it!

[![Visits Badge](https://badges.pufler.dev/visits/sanelk2004/sanelk2004?style=for-the-badge)](https://github.com/sanelk2004/sanelk2004/)

<p align="left" style="font-size=10px;"><i>This README was last generated on Wednesday, October 6, 2021, 10:27 PM EDT</i></p><p align="right" style="font-size: 20px;"><a style="font-size: 20px;" href="https://medium.com/swlh/how-to-create-a-self-updating-readme-md-for-your-github-profile-f8b05744ca91">How this was made</a></p>
